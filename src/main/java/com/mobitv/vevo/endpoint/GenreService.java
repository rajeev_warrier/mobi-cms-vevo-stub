package com.mobitv.vevo.endpoint;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobitv.aaa.service.impl.VevoRequestProcessor;

@Path("/v1/Mapping")
@Service("GenreService")
public class GenreService {	
	
	private static Logger logger = LoggerFactory
			.getLogger(GenreService.class);
	
	@Autowired
	private VevoRequestProcessor vevoRequestProcessor;		

	@GET
	@Path("/VideoGenre")
	@Produces({ "application/json", "application/xml","application/atom+xml", "application/x-lua","text/x-lua" })
	public  Response getGenre(@QueryParam("$skiptoken") String skipToken,@QueryParam("pkey") String partnerKey) throws Exception {
		try {
			return vevoRequestProcessor.getGenre();
		} catch (IOException e) {
			throw e;
		}	
	}		
	
}
