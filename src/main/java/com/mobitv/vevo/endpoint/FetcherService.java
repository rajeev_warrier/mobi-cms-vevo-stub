package com.mobitv.vevo.endpoint;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobitv.aaa.service.impl.VevoRequestProcessor;

@Path("/v1/Catalog")
@Service("FetchService")
public class FetcherService {	
	
	private static Logger logger = LoggerFactory
			.getLogger(FetcherService.class);
	
	@Autowired
	private VevoRequestProcessor vevoRequestProcessor;		

	@GET
	@Path("/Videos")
	@Produces({ "application/json", "application/xml","application/atom+xml","application/x-lua","text/x-lua" })
	public  Response getVideos(@QueryParam("skiptoken") String skipToken,@QueryParam("pkey") String partnerKey) throws Exception {
		try {
			return vevoRequestProcessor.getVideos(partnerKey,skipToken);
		} catch (IOException e) {
			throw e;
		}	
	}	
	
	
	@GET
	@Path("/VideoCredits")
	@Produces({ "application/json", "application/xml","application/atom+xml", "application/x-lua","text/x-lua" })
	public  Response getVideoCredits() throws Exception {
		try {
			return vevoRequestProcessor.getVideoCredit();
		} catch (IOException e) {
			throw e;
		}	
	}
	
	@GET
	@Path("/ArtistMetadatas")
	@Produces({ "application/json", "application/xml","application/atom+xml", "application/x-lua","text/x-lua" })
	public  Response getArtistMetadata() throws Exception {
		try {
			return vevoRequestProcessor.getArtistMetadata();
		} catch (IOException e) {
			throw e;
		}	
	}
	
	@GET
	@Path("/DeletedVideos")
	@Produces({ "application/json", "application/xml","application/atom+xml", "application/x-lua","text/x-lua" })
	public  Response getDeletedVideos(@QueryParam("skiptoken") String skipToken,@QueryParam("pkey") String partnerKey) throws Exception {
		try {
			return vevoRequestProcessor.getDeletedVideos(partnerKey, skipToken);
		} catch (IOException e) {
			throw e;
		}	
	}
	
}
