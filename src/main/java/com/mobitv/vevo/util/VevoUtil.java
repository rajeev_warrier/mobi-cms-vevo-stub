package com.mobitv.vevo.util;

public class VevoUtil {
	
	public static void throwExceptionIfNeeded(String url, String feed) throws Exception {
		if(url.toUpperCase().contains(feed.toUpperCase())) {
			throw new Exception();
		}
	}
	
	public static void waitTimeBeforeResponding(String responseTime) throws InterruptedException {
		Integer respTime = Integer.parseInt(responseTime);
		if(!(respTime.intValue() == -1)) {		
			Thread.sleep(respTime * 1000);			
		}
	}

}
