package com.mobitv.vevo.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.stereotype.Service;

@Service
public class FileUtil {
	
	public static String readFile( String file ) throws IOException {
	    BufferedReader reader = new BufferedReader( new FileReader (file));
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    //String         ls = System.getProperty("line.separator");

	    while( ( line = reader.readLine() ) != null ) {
	        stringBuilder.append( line );
	        stringBuilder.append( "\n" );
	    }

	    return stringBuilder.toString();
	}
	
	public static void main(String args[]) throws IOException {
		String fileContent = readFile("D:\\git-repos\\mobi-cms-vevo-stub\\src\\main\\resources\\Feed1.xml");
		System.out.println("The file content is :" + fileContent);
	}

}
