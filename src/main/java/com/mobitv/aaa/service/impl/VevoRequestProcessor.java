package com.mobitv.aaa.service.impl;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mobitv.vevo.util.FileUtil;
import com.mobitv.vevo.util.VevoUtil;

@Service
public class VevoRequestProcessor {
	
	@Value("${fetch.feed.path}")
	private String filePath; 
	
	@Value("${deleted.feed.path}")
	private String deletedFilePath;
	
	@Value("${feed.to.be.errored.out}")
	private String feedToBeErroredOut;
	
	@Value("${base.feed}")
	private String baseFeed;
	
	@Value("${base.deleted.feed}")
	private String baseDeletedFeed;
	
	@Value("${response.time}")
	private String responseTime;
	
	@Value("{deleted.feed.to.be.errored.out}")
	private String deletedFeedToBeErroredOut;
	
	public Response getVideos(String partnerKey, String skipToken) throws IOException, Exception {
		String filePath = null;
		if(!StringUtils.isEmpty(skipToken)) {
			VevoUtil.throwExceptionIfNeeded(skipToken, feedToBeErroredOut);
			filePath = this.filePath + "/" + skipToken;			
		} else {
			if(baseFeed.toUpperCase().equalsIgnoreCase(feedToBeErroredOut)) {
				throw new Exception();
			}
			filePath = this.filePath + "/" + baseFeed;
		}
		String fileContent = FileUtil.readFile(filePath);
		VevoUtil.waitTimeBeforeResponding(responseTime);
		return Response.ok(fileContent).build();		
	}
	
	public Response getVideoCredit() throws Exception {
		String filePath = this.filePath + "/VideoCredit.xml";		
		String fileContent = FileUtil.readFile(filePath);
		VevoUtil.waitTimeBeforeResponding(responseTime);
		return Response.ok(fileContent).build();		
	}
	
	public Response getArtistMetadata() throws Exception {
		String filePath = this.filePath + "/ArtistMetadata.xml";		
		String fileContent = FileUtil.readFile(filePath);
		VevoUtil.waitTimeBeforeResponding(responseTime);
		return Response.ok(fileContent).build();		
	}
	
	public Response getGenre() throws Exception {
		String filePath = this.filePath + "/Genre.xml";		
		String fileContent = FileUtil.readFile(filePath);
		VevoUtil.waitTimeBeforeResponding(responseTime);
		return Response.ok(fileContent).build();		
	}
	
	public Response getDeletedVideos(String partnerKey, String skipToken) throws Exception {
		String filePath = this.deletedFilePath + "/" + baseDeletedFeed;		
		if(!StringUtils.isEmpty(skipToken)) {
			VevoUtil.throwExceptionIfNeeded(skipToken, deletedFeedToBeErroredOut);
			filePath = this.deletedFilePath + "/" + skipToken;			
		} else {
			if(baseDeletedFeed.toUpperCase().equalsIgnoreCase(deletedFeedToBeErroredOut)) {
				throw new Exception();
			}
			filePath = this.deletedFilePath + "/" + baseDeletedFeed;
		}
		String fileContent = FileUtil.readFile(filePath);
		VevoUtil.waitTimeBeforeResponding(responseTime);
		return Response.ok(fileContent).build();	
	}
	
	
	
}
